﻿'use strict';

hbApp.factory('hbFactory', function () {
    return {
        
        questions: [{
            questionid: 1,
            question: 'Sex?',
            answer:["Male","Female"]
        },
        {
            questionid: 2,
            question: 'Pregnant/ breast feeding?',
            answer: ["Yes", "No"]
        },
         {
             questionid: 3,
             question: 'Age',
             answer: ["18-30", "30-40","40-60","60-70","70+"]
         },
          {
              questionid: 4,
              question: 'Veg (per day)',
              answer: ["0-1", "2-3", "4-5", "6-7", "8+"]
          },
          {
              questionid: 5,
              question: 'Fruit (per day)',
              answer: ["0-1", "2-3", "4-5", "6-7", "8+"]
          },
        ]
    }
})

hbApp.factory('hbFactoryScreens', function () {
    return {
        screens: [
            {
                screenid: 1,
                questions: [{
                    questionid: 1,
                    question: 'Sex?',
                    option: [{optionid:1,optionName:"Male"}, {optionid:2,optionName:"Female"}],
                    varient: [],
                    description:"",
                    answer:''
                }]
            },
            {
                screenid: 2,
                questions: [{
                    questionid: 2,
                    question: 'Pregnant/ breast feeding?',
                    option: [{ optionid: 1, optionName: "Yes" }, { optionid: 2, optionName: "No" }],
                    varient: [],
                    description: "",
                    answer:''
                }]
            },
             {
                 screenid: 3,
                 questions: [{
                     questionid: 3,
                     question: 'Age?',
                     option: [{ optionid: 1, optionName: "18-30" }, { optionid: 2, optionName: "30-40" }, { optionid: 3, optionName: "40-60" }, { optionid: 4, optionName: "60-70" }, { optionid: 5, optionName: "70+" }],
                     varient: [],
                     description: "",
                     answer:''
                 }]
             },
              {
                  screenid: 3,
                  questions: [{
                      questionid: 4,
                      question: 'Height?',
                      option: [""],
                      varient: ['cm', 'feet', 'inches'],
                      description: "",
                      answer: ''
                  }]
              },
              {
                  screenid: 3,
                  questions: [{
                      questionid: 5,
                      question: 'Weight?',
                      option: [""],
                      varient: ['kg', 'stone'],
                      description: "",
                      answer: ''
                  }]
              },
              {
                  screenid: 4,
                  questions: [{
                      questionid: 6,
                      question: 'Veg (per day)',
                      option: [{ optionid: 1, optionName: "0-1" }, { optionid: 2, optionName: "2-3" }, { optionid: 3, optionName: "4-5" }, { optionid: 4, optionName: "6-7" }, { optionid: 5, optionName: "8+" }],
                      varient: ['per Day'],
                      description: "Portions per day - One adult portion of fruit or vegetables is 80g (NHS Choices). Describe as 'fist-sized'",
                      answer: ''
                  }]
              },
              {
                  screenid: 4,
                  questions: [{
                      questionid: 7,
                      question: 'Fruit (per day)',
                      option: [{ optionid: 1, optionName: "0-1" }, { optionid: 2, optionName: "2-3" }, { optionid: 3, optionName: "4-5" }, { optionid: 4, optionName: "6-7" }, { optionid: 5, optionName: "8+" }],
                      varient: ['per Day'],
                      description: "Portions per day - One adult portion of fruit or vegetables is 80g (NHS Choices). Describe as 'fist-sized'",
                      answer: ''
                  }]
              },
              {
                  screenid: 4,
                  questions: [{
                      questionid: 8,
                      question: 'Leafy greens (per day)',
                      option: [{ optionid: 1, optionName: "0-1" }, { optionid: 2, optionName: "2-3" }, { optionid: 3, optionName: "4-5" }, { optionid: 4, optionName: "6-7" }, { optionid: 5, optionName: "8+" }],
                      varient: ['per Day'],
                      description: "Portions per day - One adult portion of fruit or vegetables is 80g (NHS Choices)",
                      answer: ''
                  }]
              },

               {
                   screenid: 4,
                   questions: [{
                       questionid: 9,
                       question: 'Whole grain (per day)',
                       option: [{ optionid: 1, optionName: "0-1" }, { optionid: 2, optionName: "2-3" }, { optionid: 3, optionName: "4-5" }, { optionid: 4, optionName: "6-7" }, { optionid: 5, optionName: "8+" }],
                       varient: ['per Day'],
                       description: "Breakfast cereal: One tablespoon uncooked oats, Tbsp wholegrain cereal Bread & Crackers: One medium slice bread, 1/2 tortilla, 1/2 pitta, two rye crisp bread, two oatcakes Meals: Two heaped Tbsp cooked brown rice, three Tbsp wholegrain pasta   Snacks: 1/2 scone, two oatcakes, two to three cups plain popcorn",
                       answer: ''
                   }]
               },
               {
                   screenid: 4,
                   questions: [{
                       questionid: 10,
                       question: 'Fish (per week) (oily fish / fish / seafood split)',
                       option: [{ optionid: 1, optionName: "0-1" }, { optionid: 2, optionName: "2-3" }, { optionid: 3, optionName: "4-5" }, { optionid: 4, optionName: "5+" }],
                       varient: ['per Week'],
                       description: "A portion of oily fish/ white fish/ seafood is around 140g (4.9oz) At least one oily fish portion per week At least two portions a week",
                       
                       answer: ''
                   }]
               },
                {
                    screenid: 4,
                    questions: [{
                        questionid: 11,
                        question: 'Nuts & seeds (per week)',
                        option: [{ optionid: 1, optionName: "0-1" }, { optionid: 2, optionName: "2-3" }, { optionid: 3, optionName: "4-5" }, { optionid: 4, optionName: "5-6" }, { optionid: 5, optionName: "6-7" },  { optionid: 6, optionName: "7+" }],
                        varient: ['per Week'],
                        description: "A portion is around 28 grams; describe as 'fits in palm of hand'",

                        answer: ''
                    }]
                },
                {
                    screenid: 4,
                    questions: [{
                        questionid: 12,
                        question: 'Water (glasses per day)',
                        option: [{ optionid: 1, optionName: "1-3" }, { optionid: 2, optionName: "4-6" }, { optionid: 3, optionName: "7-9" }, { optionid: 4, optionName: "10-13" }, { optionid: 5, optionName: "13+" }],
                        varient: ['per Day'],
                        description: "1 glass is 250ml",
                        answer: ''
                    }]
                },
                {
                    screenid: 4,
                    questions: [{
                        questionid: 13,
                        question: 'Additional fluid (cups per day)',
                        option: [{ optionid: 1, optionName: "1-3" }, { optionid: 2, optionName: "4-6" }, { optionid: 3, optionName: "7-9" }, { optionid: 4, optionName: "10-13" }, { optionid: 5, optionName: "13+" }],
                        varient: ['per Day'],
                        description: "1 cup is 230ml e.g. herbal tea, fruit juice, sqush, tea, coffee",
                        answer: ''
                    }]
                },
                {
                    screenid: 4,
                    questions: [{
                        questionid: 14,
                        question: 'Dairy (per day)',
                        option: [{ optionid: 1, optionName: "0" }, { optionid: 2, optionName: "0.5" }, { optionid: 3, optionName: "1" }, { optionid: 4, optionName: "2" }, { optionid: 5, optionName: "3+" }],
                        varient: ['per Day'],
                        description: "1 portion is a cup which is 230ml (or size of tennis ball) 1 cup milk or buttermilk  1 cup yogurt 1 1/2 ounces natural cheese 2 ounces processed cheese 2 cups cottage cheese (it’s lower in calcium than most other cheeses) 1/2 cup ricotta cheese 1/2 cup dry nonfat milk 1/2 cup evaporated milk 1 cup frozen yogurt or 1 1/2 cups ice milk ",
                        answer: ''
                    }]
                },

                {
                    screenid: 4,
                    questions: [{
                        questionid: 15,
                        question: 'Eggs (per week)',
                        option: [{ optionid: 1, optionName: "0" }, { optionid: 2, optionName: "1-2" }, { optionid: 3, optionName: "2-4" }, { optionid: 4, optionName: "4-6" },  { optionid: 5, optionName: "7+" }],
                        varient: ['per Week'],
                        description: "No recommended number of eggs when eaten in a balanced diet",
                        answer: ''
                    }]
                },

                 {
                     screenid: 4,
                     questions: [{
                         questionid: 16,
                         question: 'Eggs (per week)',
                         option: [{ optionid: 1, optionName: "" }, { optionid: 2, optionName: "1" }, { optionid: 3, optionName: "2" }, { optionid: 5, optionName: "3+" }],
                         varient: ['per Day'],
                         description: "1 portion is a cup which is 230ml (or size of tennis ball) 1 cup milk or buttermilk  1 cup yogurt 1 1/2 ounces natural cheese 2 ounces processed cheese 2 cups cottage cheese (it’s lower in calcium than most other cheeses) 1/2 cup ricotta cheese 1/2 cup dry nonfat milk 1/2 cup evaporated milk 1 cup frozen yogurt or 1 1/2 cups ice milk ",
                         answer: ''
                     }]
                 },
        ]
    }
});