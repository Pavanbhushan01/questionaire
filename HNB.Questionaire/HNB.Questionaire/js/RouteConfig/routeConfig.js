﻿'use strict';
hbApp.config(function ($routeProvider) {
    $routeProvider.when('/', { templateUrl: 'templates/Section1Welcome.html', controller: 'hbCtrl1Welcome' })
    .when('/Gender', { templateUrl: 'templates/Section2Screen1.html', controller: 'hbCtrl2Gender' })
    .when('/General', { templateUrl: 'templates/Section3General.html', controller: 'hbCtrl4General' })
    .when('/Female', { templateUrl: 'templates/Section3Female.html', controller: 'hbCtrl3Female' })
    .when('/Diet', { templateUrl: 'templates/Section4Diet.html', controller: 'hbCtrl4General' })
    .otherwise({redirectTo:'/'})
})