﻿'use strict';
hbApp.directive('hbbackbtn', function () {
    return {
        restrict:'AE',
          
        template: "<a class='btn btn-success'>Back</a>",
        link: function(scope,element, attrs,controller){
            element.on('click', function () {
                history.back();
                scope.$apply();
            })
    }
           
        }

   
})