﻿hbApp.controller('hbCtrl1Welcome', function ($scope) {
    $scope.welcomeMessage = "Welcome to H&B Questionaire"
    
    $scope.bodyMessage1 = "The vision for project milo is to create a truly unique health and well-being experience";
    $scope.bodyMessage2 = "We will make a material difference to health, by building a unique, curated health and well-being experience, which gives customers greater control over both their health and the health of their families.";
    $scope.bodyMessage3="Holland & Barrett is the one place I go for health and wellbeing information, advice, community engagement, and products and services from different sources";

});